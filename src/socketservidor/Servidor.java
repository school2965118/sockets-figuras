package socketservidor;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import javax.swing.JDialog;
import javax.swing.JPanel;

public class Servidor extends JDialog {
    private final int PUERTO = 3333;
    private ServerSocket serverSocket = null;
    private DataInputStream entrada;
    Mensajes hiloMensaje;
    MyPanel myPanel = new MyPanel();

     public Servidor(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        hiloMensaje = new Mensajes(myPanel); // Pasamos la instancia del panel al hilo
        hiloMensaje.start();
        getContentPane().add(myPanel, BorderLayout.CENTER);
        revalidate();
        repaint();
        setSize(500, 500);
    }


    class Mensajes extends Thread {

        MyPanel myPanel;

        public Mensajes(MyPanel panel) {
            this.myPanel = panel;
        }

        public void run() {
            try {
                serverSocket = new ServerSocket(PUERTO);
                System.out.println("Se inicio el sistema correctamente");

                while (true) {
                    Socket clientSocket = serverSocket.accept();
                    // Crear un nuevo hilo para cada conexión de cliente
                    new Thread(() -> {
                        try {
                            DataInputStream entrada = new DataInputStream(new BufferedInputStream(clientSocket.getInputStream()));
                            System.out.println("Se recibio conexion con el cliente");
                            String task;
                            while ((task = entrada.readLine()) != null) { // Continúa leyendo tareas hasta que se cierre la conexión
                                if (task.equals("Exit")) {
                                    clientSocket.close();
                                    System.out.println("Se cerro la conexion con el cliente");
                                    break;
                                }
                                System.out.println("Se recibio la tarea " + task);
                                myPanel.processTask(task);
                            }
                            entrada.close();
                        } catch (IOException e) {
                            System.err.println("Error en la conexión:" + e.getMessage());
                            e.printStackTrace();
                        }
                    }).start();
                }
            } catch (IOException e) {
                System.err.println("Error en la conexión:" + e.getMessage());
                e.printStackTrace();
            }
        }
    }
    
    class MyPanel extends JPanel {

        private String task;

        public void processTask(String task) {
            this.task = task;
            System.out.println("Procesando tarea: " + task);
            repaint();
            System.out.println("Tarea: " + task + " terminada.");
        }

        @Override
        protected void paintComponent(Graphics g) {
            super.paintComponent(g);
            if (task != null) {
                // obtén el color de la segunda cifra de la tarea
                char colorCode = task.charAt(1);

                switch (colorCode) {
                    case '1':
                        g.setColor(Color.BLUE);
                        break;
                    case '2':
                        g.setColor(Color.GREEN);
                        break;
                    case '3':
                        g.setColor(Color.RED);
                        break;
                    default:
                        g.setColor(Color.BLACK);
                        break;
                }

                // obtén la forma de la primera cifra de la tarea
                char shapeCode = task.charAt(0);

                // Calcula las coordenadas para centrar la figura
                int centerX = (this.getWidth() - 250) / 2;
                int centerY = (this.getHeight() - 250) / 2;

                switch (shapeCode) {
                    case '1':
                        g.fillOval(centerX, centerY, 250, 250);
                        break;
                    case '2':
                        g.fillRect(centerX, centerY, 250, 250);
                        break;
                    default:
                        break;
                }
            }
        }
    }
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Servidor.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Servidor.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Servidor.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Servidor.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                Servidor dialog = new Servidor(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}


